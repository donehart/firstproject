//
//  WeSplitApp.swift
//  WeSplit
//
//  Created by Мохьмад Висаитов on 01.12.2022.
//

import SwiftUI

@main
struct WeSplitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
